# generateUniqId

A plugin for LimeSurvey to generate an unique id for usage in survey.

## Installation

See [Install and activate a plugin for LimeSurvey](https://extensions.sondages.pro/about/install-and-activate-a-plugin-for-limesurvey.html).

## Usage

- Activate the plugin
- Create a short text question type
- In _Input_ set _Use this question as an unique id._ to Yes
- Use this unique id question like all other Expression Manager variables.
- Optionnaly : you can add a prefix to the generated unique ID
- Ypu can choose to use saved id in place of random string. Use it with care.

_tip_ You can use hidden attribute to totally hide this question to participant

## Contribute

Issue and pull request are welcome on [gitlab](https://gitlab.com/SondagesPro/QuestionSettingsType/generateUniqId).

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2019-2021 Denis Chenu <https://sondages.pro>

Distributed under [GNU AFFERO GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/agpl.txt) licence
