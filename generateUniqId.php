<?php

/**
 * generateUniqId
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019-2024 Denis Chenu <http://www.sondages.pro>
 * @license AGPL v3
 * @version 0.4.2
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class generateUniqId extends PluginBase
{
    protected static $description = 'Generate an unique id in a short text question.';
    protected static $name = 'generateUniqId';

    /** @var integer|null keep track of surveyId **/
    private $surveyId;

    /** @inheritdoc, this plugin do not have public method */
    public $allowedPublicMethods = [];

    /** @inheritdoc **/
    function init()
    {
        $this->subscribe('beforeQuestionRender', 'hideAnswerPart');
        $this->subscribe('newQuestionAttributes', 'addUniqueIdAttributes');
        /* in beforeSurveyPage : best way */
        $this->subscribe('beforeSurveyPage', 'beforeSurveyPage');
    }

    /* Add the uniqueId attribute in registered attributes */
    public function addUniqueIdAttributes()
    {
        $attributeEvent = $this->getEvent();
        $scriptAttributes = array(
            'useAsUniqueId' => array(
                'name'      => 'useAsUniqueId',
                'types'     => 'S', /* Only for short text */
                'category'  => $this->translate('Unique id'),
                'sortorder' => 200,
                'inputtype' => 'switch',
                'options' => array(
                    0 => gT('No'),
                    1 => gT('Yes')
                ),
                'caption'   => $this->translate('Use this question as an unique id.'),
                'help'   => $this->translate('An unique id was generated and fill the answer of this question. This unique id can be used after in expression manager. This unique id can not be updated by user.'),
                'default'   => 0,
            ),
            'uniqueIdPrefix' => array(
                'name'      => 'uniqueIdPrefix',
                'caption'   => $this->translate('Optionnal prefix for unique id.'),
                'help'   => null,
                'types'     => 'S', /* Only for short text */
                'category'  => $this->translate('Unique id'),
                'sortorder' => 210,
                'inputtype' => 'textarea',
                'default' => '',
                'expression' => 1
            ),
            'uniqueIdType' => array(
                'name'      => 'uniqueIdType',
                'caption'   => $this->translate('Type of unique ID.'),
                'help'   => $this->translate("Using saved id are lesser secure than the randme id"),
                'types'     => 'S', /* Only for short text */
                'category'  => $this->translate('Unique id'),
                'sortorder' => 220,
                'inputtype' => 'singleselect',
                'options' => array(
                    'random' => $this->translate("A random generated unique ID with uniqid function."),
                    'savedid' => $this->translate("The saved id, warning didn't work at 1st call."),
                ),
                'default' => '',
                'expression' => 0
            ),
            'savedidPad' => array(
                'name'      => 'savedidPad',
                'caption'   => $this->translate('Is use save id, pad with 0 lenght'),
                'help'   => null,
                'types'     => 'S', /* Only for short text */
                'category'  => $this->translate('Unique id'),
                'sortorder' => 230,
                'inputtype' => 'integer',
                'min' => 0,
                'default' => '0',
            ),
        );
        if (version_compare(App()->getConfig('versionnumber'), "3.6.0") < 1) {
            // Remove if version is under 3.6
            unset($scriptAttributes['uniqueIdPrefix']);
        }
        $attributeEvent->append('questionAttributes', $scriptAttributes);
    }

    /* Hide the answer part according to attribute settings */
    public function hideAnswerPart()
    {
        if (!$this->getEvent()) {
            throw new CHttpException(403);
        }
        $beforeQuestionEvent = $this->getEvent();
        $aAttributes = QuestionAttribute::model()->getQuestionAttributes($beforeQuestionEvent->get('qid'));
        if (!empty($aAttributes['useAsUniqueId'])) {
            $sid = $beforeQuestionEvent->get('surveyId');
            $questionColumn = $sid . 'X' . $beforeQuestionEvent->get('gid') . 'X' . $beforeQuestionEvent->get('qid');
            /* No need to check else there are an issue … */
            $beforeQuestionEvent->set('answers', CHtml::hiddenField(
                $questionColumn,
                $_SESSION['survey_' . $sid][$questionColumn],
                array('id' => 'answer' . $questionColumn)
            ));
        }
    }

    /* setAndFixUniqId */
    public function beforeSurveyPage()
    {
        $surveyId = $this->getEvent()->get('surveyId');
        if (empty($_SESSION['survey_' . $surveyId]['insertarray'])) {
            /* Last chance for all in one survey of for question in 1st group */
            /* Happen for all page before page is shown, don't broke on version lesser than 3 since event didn't exist */
            $this->surveyId = $surveyId;
            $this->subscribe('getPluginTwigPath', 'getPluginTwigPath');
            return;
        }
        if (!isset($_SESSION['survey_' . $surveyId]['generatedUniqId'])) {
            $this->surveyId = $surveyId;
            $this->setUniqId();
        }
        if (empty($_SESSION['survey_' . $surveyId]['generatedUniqId'])) {
            return;
        }

        /* disable POST value update of uniqId question */
        $generatedUniqId = $_SESSION['survey_' . $surveyId]['generatedUniqId'];
        foreach ($generatedUniqId as $columnUniqId) {
            if (App()->getRequest()->getPost($columnUniqId)) {
                $_POST[$columnUniqId] = $_SESSION['survey_' . $surveyId][$columnUniqId];
            }
        }
    }
    /* setAndFixUniqId */
    public function getPluginTwigPath()
    {
        $surveyId = $this->surveyId;
        if (!empty($_SESSION['survey_' . $surveyId]['srid'])) {
            $this->setUniqId();
            $this->unsubscribe('getPluginTwigPath');
        }
    }

    /**
     * Set the unique id for all needed question for current survey
     **/
    public function setUniqId()
    {
        if (!$this->surveyId) {
            throw new CHttpException(403);
        }
        $surveyId = $this->surveyId;
        if (isset($_SESSION['survey_' . $surveyId]['generatedUniqId'])) {
            return;
        }
        if (intval(Yii::app()->getConfig('versionnumber')) > 3) {
            $aQuestionsUniqueId = Question::model()->with('questionattributes')->findAll(
                'questionattributes.attribute = :attribute AND questionattributes.value = :value and sid =:sid',
                array(
                    ':attribute' => 'useAsUniqueId',
                    ':value' => 1,
                    ':sid' => $surveyId
                )
            );
        } else {
            $criteria = new CDbCriteria();
            $criteria->join = 'LEFT JOIN {{question_attributes}} as question_attributes ON question_attributes.qid=t.qid';
            $criteria->condition = 't.sid = :sid and attribute = :attribute AND value = :value';
            $criteria->params = array(':sid' => $surveyId,':attribute' => 'useAsUniqueId',':value' => 1);
            $aQuestionsUniqueId = Question::model()->findAll($criteria);
        }
        $srid = isset($_SESSION['survey_' . $surveyId]['srid']) ? $_SESSION['survey_' . $surveyId]['srid'] : null;
        $oResponse = null;
        if ($srid && Survey::model()->findByPk($surveyId)->active == "Y") {
            $oResponse = Response::model($surveyId)->findByPk($srid);
        }
        $generatedUniqId = array();
        if (!empty($aQuestionsUniqueId)) {
            foreach ($aQuestionsUniqueId as $oQuestionUniqueId) {
                $questionColumn = $oQuestionUniqueId->sid . 'X' . $oQuestionUniqueId->gid . 'X' . $oQuestionUniqueId->qid;
                $generatedUniqId[] = $questionColumn;
                if (empty($oResponse) || empty($oResponse->$questionColumn)) {
                    $tempUniqueId = hash('sha256', uniqid($srid . rand(), true));
                    $oUniqueIdType = QuestionAttribute::model()->find(
                        "qid = :qid and attribute = :attribute",
                        array(':qid' => $oQuestionUniqueId->qid, ':attribute' => 'uniqueIdType' )
                    );
                    if ($oUniqueIdType && $oUniqueIdType->value == 'savedid') {
                        $tempUniqueId = strval($srid);
                        $pad = 0;
                        $oSavedidPad = QuestionAttribute::model()->find(
                            "qid = :qid and attribute = :attribute",
                            array(':qid' => $oQuestionUniqueId->qid, ':attribute' => 'savedidPad' )
                        );
                        if ($oSavedidPad && intval($oSavedidPad->value) > 0) {
                            $pad = intval($oSavedidPad->value);
                            $tempUniqueId = str_pad($srid, $pad, "0", STR_PAD_LEFT);
                        }
                    }
                    if (version_compare(App()->getConfig('versionnumber'), "3.6.0") > 0) {
                        $oPrefixUniqueId = QuestionAttribute::model()->find(
                            "qid = :qid and attribute = :attribute",
                            array(':qid' => $oQuestionUniqueId->qid, ':attribute' => 'uniqueIdPrefix' )
                        );
                        if ($oPrefixUniqueId && $oPrefixUniqueId->value) {
                            $tempUniqueId = LimeExpressionManager::ProcessStepString($oPrefixUniqueId->value, array(), 3, true) . $tempUniqueId;
                        }
                    }
                    $_SESSION['survey_' . $surveyId][$questionColumn] = $tempUniqueId;
                    if ($oResponse) {
                        $oResponse->$questionColumn = $_SESSION['survey_' . $surveyId][$questionColumn];
                    }
                } else {
                    $_SESSION['survey_' . $surveyId][$questionColumn] = $oResponse->$questionColumn;
                }
            }
            if ($oResponse) {
                $oResponse->save();
            }
        }
        $_SESSION['survey_' . $surveyId]['generatedUniqId'] = $generatedUniqId;
    }

    /**
    * Alternative of 3.X parent::gT
    * To be lesser than 3.X compatible
    */
    private function translate($string, $escapeMode = 'unescaped', $language = null)
    {
        if (method_exists($this, "gT")) {
            return $this->gT($string, $escapeMode, $language);
        }
        return $sToTranslate;
    }
}
